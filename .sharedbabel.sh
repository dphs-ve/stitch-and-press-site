# ONLY FOR USE BY source!
echo "Discovering _babels..."
unset inputbabel outputbabel
inputbabel=()
outputbabel=()
while IFS= read -r -d $'\0' file; do
    inputbabel+=("$file")
    ob=$(echo "$file" | sed 's/_babel/babel/')
    outputbabel+=("$ob")
    mkdir -p "$ob"
    echo "- $file -> $ob"
done < <(find . -name _babel -print0)
echo "Done discovering. $(declare -p inputbabel) $(declare -p outputbabel)"
