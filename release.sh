#!/usr/bin/env bash
echo "Building everything with ./build.sh..."
./build.sh || exit 1
echo "Uploading _site to the server..."
./uploadtoserver.sh "$@" || exit 1
echo "Done!"
