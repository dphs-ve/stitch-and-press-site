#!/usr/bin/env bash
echo "Welcome to the automated FTP uploader."
username="$1"
pass="$2"
relsite="$3"
if [ "x$username" == "x" ]; then
    printf "Username: "
    read username
fi
if [ "x$pass" == "x" ]; then
    stty -echo
    printf "Password: "
    read pass
    stty echo
    printf "\n"
fi
if [ "x$relsite" == "x" ]; then
    printf "Directory: "
    read relsite
fi
echo "Uploading now..."
cd _site
# chmod everything
chmod -R a+rX .
# login and put _site
set -o verbose
lftp -u "$username","$pass" dphsrop.com << EOT
set ssl:verify-certificate false
mirror -R --delete-first --verbose=3 . ${relsite}
EOT
set +o verbose
echo "Completed."
