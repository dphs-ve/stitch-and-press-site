#!/usr/bin/env bash
command_exists () {
	command -v "$1" > /dev/null 2>&1
}
echo "Please enter your password to allow sudo."
while true; do
printf "[pseudo] password for $(whoami): "
stty -echo
read password
stty echo
echo "" # for extra newline with echo off
# expire sudo here for testing
sudo -k
good="$(echo "$password" | sudo -S -p '' echo "ok" 2>/dev/null || echo "bad")"
if [[ "$good" == "ok" ]]; then
    break
else
    echo "Sorry, try again."
fi
done
echo "Password verified."
if ! command_exists python3; then
    echo "Requires python3."
elif ! command_exists pip3; then
    echo "Requires pip3."
else
    pip3 install sh
    python3 _installer.py "$password"
fi
