define(["jquery", "jqueryui", "react", "reactbs", "store/babel/realdata", "babel/ui", "internal/firebase", "slider", "store/babel/editor", "jqueryhashchange"],
    function defineUI($, jqui, React, ReactBS, products, ui, fb, slider, editor) {
        var exports = {};
        exports.renderProducts = function renderProducts() {
            var psl = new slider.PagesSlider($(".slider"));
            psl.init();
            var STORE = 0, PRODUCT = 1;
            var lhs = -1;
            var callback = function whenProductsLoaded(fastDisplay) {
                ui.stopLoading("#panel", function clearLoader(whenDone) {
                    whenDone();
                    $(".cssload-loader").remove();
                }, fastDisplay);
            };
            var firstDisplay = true;
            var isFirstDisplay = function isFirstDisplayThenReset() {
                if (firstDisplay) {
                    firstDisplay = false;
                    return true;
                }
                return false;
            };
            var showOneProduct = function showOneProduct(product) {
                lhs = PRODUCT;
                var firstDisplay = isFirstDisplay();
                ui.startLoading("#panel", function onReady(whenComplete) {
                    whenComplete();
                    React.render(<products.Product item={ product }
                                                   showStore={restoreStore} singular={true}/>,
                        document.getElementById("product-panel"),
                        function rendered() {
                            callback(!firstDisplay);
                        });
                }, !firstDisplay);
            };
            var lastHash = undefined;
            var preEditorHashString = undefined;
            var showEditor = function showEditor(product, data) {
                editor.display(product, function readyForPresenting() {
                    editor.update(data);
                    psl.next(function showStoreWhileHidden() {
                        $("#product-panel").empty();
                        displayStore(isFirstDisplay());
                    });
                });
            };
            var hideEditor = function hideEditor() {
                psl.prev();
                editor.hide();
            };
            $(window).hashchange(function displayHashProduct() {
                var hash;
                try {
                    hash = window.location.hash.substring(1);
                    if (hash === lastHash) {
                        // -.-
                        return;
                    }
                    var hashData = hash.split("@");
                    if (hash === preEditorHashString) {
                        // Same as pre-editor state, just slide it in.
                        hideEditor();
                        preEditorHashString = undefined;
                        return;
                    }
                    var product = hashData[0];
                    var view = hashData.length < 2 ? undefined : hashData[1];
                    var editorData = hashData.length < 3 ? undefined : hashData[2];
                    if (product === "thankyou") {
                        // not a product :3
                        restoreStore();
                        return;
                    }
                    if (!product) {
                        restoreStore();
                        return;
                    }
                    var lastHashData = lastHash ? lastHash.split("@") : undefined;
                    var lastView = !lastHash || lastHashData.length < 2
                        ? undefined
                        : lastHashData[1];
                    if (view === "editor") {
                        if (lastView === "editor") {
                            editor.update(editorData);
                            return;
                        } else {
                            preEditorHashString = lastHash || "";
                        }
                    } else {
                        preEditorHashString = undefined;
                    }
                    fb.thisFB.child("products").child(product).once("value",
                        function showProduct(p) {
                            var val = p.val();
                            if (!val) {
                                restoreStore();
                                return;
                            }
                            var productPayload = $.extend({itemId: product}, val);
                            if (view === "store" || !view) {
                                if (psl.currentIndex == 1) {
                                    // Editor visible, hide it
                                    hideEditor();
                                }
                                showOneProduct(productPayload);
                            } else if (view === "editor") {
                                showEditor(productPayload, editorData);
                            }
                        }
                    );
                } finally {
                    lastHash = hash;
                }
            });
            var restoreStore = function showStoreFromProduct() {
                var firstDisplay = isFirstDisplay();
                if (psl.currentIndex == 1) {
                    // Editor visible, hide it
                    hideEditor();
                }
                if (lhs == STORE) {
                    // No need to load what's already there.
                    return;
                }
                ui.startLoading("#panel", function onReady(whenComplete) {
                    whenComplete();
                    $("#product-panel").empty();
                    displayStore(firstDisplay);
                }, !firstDisplay);
            };
            var displayStore = function showStore(firstDisplay) {
                lhs = STORE;
                React.render(
                    <div>
                        <div style={{textAlign: "center"}}><h4>Stitch & Press Store</h4></div>
                        <products.Products callback={callback.bind(this, !firstDisplay)}/>
                    </div>,
                    document.getElementById("product-panel")
                );
            };
            $("#panel").append("<div id='product-panel' class='rendered' " +
                "style='display: none'></div>");
            ui.startsAsLoader("#panel");
            var $window = $(window);
            $window.hashchange();
            $window.on("popstate", function onPopState() {
                $window.hashchange();
            });
        };
        return exports;
    })
;
