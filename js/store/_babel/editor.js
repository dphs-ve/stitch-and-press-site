define(["jquery", "react", "reactbs", "internal/encoding", "babel/ui"],
    function defineEditorModule($, React, ReactBS, enc, ui) {
        var exports = {};
        var $editor = $("#editor");
        exports.display = function showEditorForProduct(product, onFinish) {
            var str = JSON.stringify(product, null, 4);
            var split = str.split("\n");
            var join = [];
            split.forEach(function (t) {
                join.push(<div>{t}<br/></div>);
            });
            $editor.show();
            React.render(<pre>
                {join}
            </pre>, $editor[0], function rendered() {
                onFinish();
            });
        };
        exports.hide = function invisibleEditor() {
            $editor.hide();
        };
        exports.update = function updateEditor(data) {
            var stuff = React.renderToStaticMarkup(<div>{"Update! " + JSON.stringify(data)}<br/></div>);
            $editor.append(stuff);
        };
        return exports;
    }
);
