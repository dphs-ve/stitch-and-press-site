define(["react", "reactbs", "babel/ui"], function definePlaceholderModule(React, ReactBS, ui) {
    var exports = {};
    exports.Button = React.createClass({
        handleClick: function buttonClicked() {
            ui.pushAlert("You bought a " + this.props.itemName + " for $" + this.props.itemPrice);
        },
        render: function renderFakeButton() {
            return <ReactBS.Button bsStyle="success"
                                   onClick={this.handleClick}>
                Buy me!
            </ReactBS.Button>;
        }
    });
    exports.Product = React.createClass({
        render: function renderFakeProduct() {
            var name = this.props.itemName;
            var price = this.props.itemPrice;
            return <div className="media">
                <div className="media-left">
                    <img className="media-object"
                         src="http://www.gemologyproject.com/wiki/images/5/5f/Placeholder.jpg"
                         style={{width: 64, height: 64}}/>
                </div>
                <div className="media-body">
                    <h4 className="media-heading">
                        { name }
                    </h4>
                    { this.props.itemDesc }
                </div>
                <div className="media-right">
                    <exports.Button itemName={ name } itemPrice={ price }/>
                </div>
            </div>;
        }
    });
    exports.Products = React.createClass({
        render: function renderMultipleProducts() {
            var c = this.props.count;
            var parts = [];
            for (var i = 0; i < c; i++) {
                parts.push(<exports.Product
                    itemName="Placeholder"
                    itemDesc="It holds your place very well."
                    itemPrice="0.00"
                    key={ i }/>);
            }
            return <div>{parts}</div>;
        }
    });
    return exports;
});
