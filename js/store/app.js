requirejs(["jquery", "store/babel/ui", "babel/ui"], function rqLoad($, ui, normalUi) {
    $(function onReady() {
        ui.renderProducts();
        if (window.location.hash === "#thankyou") {
            normalUi.pushAlert("Thank you for your purchase.", "success");
        }
    });
    return {};
});
