define(["jquery", "jquerytransit"], function defineSliderModule($) {
    var PagesSlider = function (slider) {
        this.slider = slider;
        this.content = slider.children().first();
        this.pages = this.content.children();

        this.currentIndex = 0;
    };
    $.extend(PagesSlider.prototype, {
        init: function () {
            //var oldT = $.fn.transition;
            //$.fn.transition = function () {
            //    console.log("Begin $.tr", arguments);
            //    return oldT.apply(this, arguments);
            //};
            var sliderWidth = this.pages.first().width();
            this.slider.width(sliderWidth);

            var totalWidth = 0;
            this.pages.each(function (index, page) {
                var $page = $(page);
                totalWidth += $page.width();
                $page.width(sliderWidth);
            });
            this.content.width(totalWidth);
            this.current();
        },
        goToIndex: function (index, cb) {
            this.content.clearQueue();
            var pg = this.pages.eq(index);
            var cnt = this.content;
            var trans = {};
            if (this.currentIndex != index) {
                var startHeight =
                    this.pages.eq(this.currentIndex).height();
                cnt.height(startHeight);
                trans.height = pg.height();
            }
            var position = pg.position();
            trans.x = -1 * position.left;
            cnt.transition(trans, 500, function callback() {
                (cb || $.noop)();
            }).transition({
                height: "initial"
            });
            this.currentIndex = index;
        },
        current: function (cb) {
            this.goToIndex(this.currentIndex, cb);
        },
        next: function (cb) {
            if (this.currentIndex >= this.pages.length - 1) {
                this.current(cb);
            } else {
                this.goToIndex(this.currentIndex + 1, cb);
            }
        },
        prev: function (cb) {
            if (this.currentIndex <= 0) {
                this.current(cb);
            } else {
                this.goToIndex(this.currentIndex - 1, cb);
            }
        }
    });
    return {PagesSlider: PagesSlider};
});