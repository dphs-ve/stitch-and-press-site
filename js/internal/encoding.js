define([], function defineEncodingModule() {
    var exports = {};
    exports.encodeObject = function encodeToJsonToB64ToURI(data) {
        try {
            var str = JSON.stringify(data);
            var b64 = btoa(str);
            var uri = encodeURIComponent(b64);
        } catch (e) {
            return "Invalid data: " + data;
        }
        return uri;
    };
    exports.decodeObject = function decodeFromURIFromB64FromJson(uri) {
        try {
            var b64 = decodeURIComponent(uri);
            var str = atob(b64);
            var data = JSON.parse(str);
        } catch (e) {
            return "Invalid data: " + uri;
        }
        return data;
    };
    return exports;
});