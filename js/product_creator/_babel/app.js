requirejs(["jquery", "babel/ui", "react", "reactbs", "internal/firebase", "noop"], function rqLoad($, ui, React, ReactBS, fb, noop) {
    $(function onReady() {
        // leet hacks?
        $.fn.serializeObject = function () {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function () {
                if (o[this.name] !== undefined) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || "");
                } else {
                    o[this.name] = this.value || "";
                }
            });
            return o;
        };
        // end hacks
        $("#panel").append("<div id='product-panel' class='rendered' " +
            "style='display: none'></div>");
        ui.startsAsLoader("#panel");
        var isAuth = window.location.search.indexOf("auth=1") > -1;
        var setupProductCreator = function setupCreator(onReady) {
            React.render(<form onSubmit={ pushProduct }>
                <ReactBS.Input name="id" type="text" label="ID" placeholder="Product ID" required/>
                <ReactBS.Input label="A or An?"/>
                <ReactBS.ButtonGroup>
                    <ReactBS.Input name="aOrAn" value="a" type="radio" label="A" required/>
                    <ReactBS.Input name="aOrAn" value="an" type="radio" label="An" required/>
                </ReactBS.ButtonGroup>
                <ReactBS.Input name="name" type="text" label="Name" placeholder="Product Name" required/>
                <ReactBS.Input name="desc" type="textarea" label="Description" placeholder="Product Description"
                               required/>
                <ReactBS.Input name="shortDesc" type="textarea" label="Short Description"
                               placeholder="Short Product Description" required/>
                <ReactBS.Input name="image" type="text" label="Image Location" placeholder="Image URL (Template)"
                               required/>
                <ReactBS.Input name="price" type="text" label="Price" placeholder="Product Price" required/>
                <ReactBS.ButtonInput type="submit" value="Submit" bsStyle="primary"
                                     className="col-md-4 col-md-offset-4" id="pc-submit"/>
            </form>, document.getElementById("product-panel"), onReady);
        };
        var pushProduct = function onSubmit(e) {
            e.preventDefault();
            var form = e.target; // probably
            var $form = $(form);
            var $submitButton = $form.find("#pc-submit");
            try {
                $submitButton.prop("disabled", true);
                var data = $form.serializeObject();
                if (!data.id || !data.aOrAn || !data.name || !data.desc || !data.shortDesc || !data.image || !data.price || !data.vei_url) {
                    ui.pushAlert("Please fill out the form!", "error");
                    $submitButton.prop("disabled", false);
                    return;
                }
                data.name = {aOrAn: data.aOrAn, name: data.name};
                var location = fb.thisFB.child("products").child(data.id);
                delete data.id;
                delete data.aOrAn;
                console.log("Set " + location.toString() + " to " + JSON.stringify(data));
                location.set(data, function onComplete(e) {
                    $submitButton.prop("disabled", false);
                    if (e) {
                        var eStr = e.toString();
                        if (!eStr.startsWith("Error: ")) {
                            eStr = "Error: " + eStr;
                        }
                        ui.pushAlert("Failed to submit. " + eStr, "error");
                    } else {
                        form.reset();
                        ui.pushAlert("Submitted successfully!", "success");
                    }
                });
            } catch (err) {
                ui.pushAlert("An error occoured: " + err, "error");
                setTimeout(function pushSecondary() {
                    ui.pushAlert("Please notify the admin.", "error");
                }, 1000);
                $submitButton.prop("disabled", false);
            }
        };
        var handleSubmit = function onSubmit(e) {
            e.preventDefault();
            try {
                var form = e.target; // probably
                var $form = $(form);
                var $submitButton = $form.find("#pc-submit");
                $submitButton.prop("disabled", true);
                var user = $form.find("input[type='email']").val();
                var pass = $form.find("input:password").val();
                if (!user || !pass) {
                    ui.pushAlert("Please fill out the form!", "error");
                    return;
                }
                fb.authenticateAdmin(user, pass, noop.noop, function authOk(data) {
                    ui.pushAlert("Logged in successfully as " + data.password.email, "success");
                    // Clear form, replace with product stuff
                    ui.startLoading("#panel", function onComplete(showLoader) {
                        var $prodPanel = $("#product-panel");
                        $prodPanel.empty();
                        showLoader();
                        window.location.search = "?auth=1";
                    });
                }, function authFailed(error) {
                    $submitButton.prop("disabled", false);
                    var eStr = error.toString();
                    if (!eStr.startsWith("Error: ")) {
                        eStr = "Error: " + eStr;
                    }
                    ui.pushAlert("Failed to log in. " + eStr, "error")
                });
            } catch (err) {
                ui.pushAlert("An error occoured: " + err, "error");
                setTimeout(function pushSecondary() {
                    ui.pushAlert("Please notify the admin.", "error");
                }, 1000);
            }
        };
        if (!isAuth && fb.thisFB.getAuth() !== null) {
            window.location.search = "?auth=1";
        }
        if (!isAuth || fb.thisFB.getAuth() === null) {
            React.render(<form onSubmit={ handleSubmit }>
                    <ReactBS.Input type="email" label="Email Address" placeholder="Enter email" required/>
                    <ReactBS.Input type="password" label="Password" placeholder="Enter password" required/>
                    <ReactBS.ButtonInput type="submit" value="Submit" bsStyle="primary"
                                         className="col-md-4 col-md-offset-4" id="pc-submit"/>
                </form>,
                document.getElementById("product-panel"),
                function onRenderComplete() {
                    ui.stopLoading("#panel", function killLoader(whenDone) {
                        whenDone();
                        $(".cssload-loader").remove();
                    });
                });
        } else {
            setupProductCreator(function afterSetup() {
                ui.stopLoading("#panel", function killLoader(whenDone) {
                    whenDone();
                    $(".cssload-loader").remove();
                });
            })
        }
    });
    return {};
});
